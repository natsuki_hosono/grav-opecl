#include <iostream>
#include <iomanip>
#include <vector>
#include <assert.h>
#include <cmath>
#include <sys/time.h>
#include <particle_simulator.hpp>
#include "class_platform.hpp"
#include "pezy.hpp"

PS::S32 DispatchKernel(const PS::S32, const PS::S32, const EPIGrav**, const PS::S32*, const EPJGrav**, const PS::S32*, const PS::SPJMonopole**, const PS::S32*);
PS::S32 RetrieveKernel(const PS::S32, const PS::S32, const PS::S32*, ForceGrav**);

void SetIC(PS::ParticleSystem<FPGrav>& ptcl){
	if(PS::Comm::getRank() != 0){
		ptcl.setNumberOfParticleLocal(0);
		return;
	}
	const int seed = 0;
	PS::MTTS mt;
	mt.init_genrand(0);
	const int Nptcl = 1024;
	ptcl.setNumberOfParticleLocal(Nptcl);
	for(PS::S32 i = 0 ; i < Nptcl ; ++ i){
		ptcl[i].mass = 1.0 / Nptcl;
		const PS::F64 radius = 3.0;
		PS::F64vec pos;
		do{
			pos.x = (2.0 * mt.genrand_res53() - 1.0) * radius;
			pos.y = (2.0 * mt.genrand_res53() - 1.0) * radius;
			pos.z = (2.0 * mt.genrand_res53() - 1.0) * radius;
		}while(pos * pos >= radius * radius);
		ptcl[i].pos = pos;
	}
}

template <class TPtclJ> class CalcGravityForce{
	static constexpr double eps2 = 1.0/32.0 * 1.0/32.0;
	public:
	void operator () (const EPIGrav* const __restrict ep_i, const PS::S32 Nip, const TPtclJ* const __restrict ep_j, const PS::S32 Njp, ForceGrav* const grav){
		for(PS::S32 i = 0; i < Nip ; ++ i){
			const EPIGrav& ith = ep_i[i];
			for(PS::S32 j = 0; j < Njp ; ++ j){
				const TPtclJ& jth = ep_j[j];
				const PS::F64vec dr = ith.pos - jth.pos;
				const PS::F64 dr2 = dr * dr + eps2;
				const PS::F64 dr_inv = 1.0 / sqrt(dr2);
				const PS::F64 m_dr3_inv = jth.mass * pow(dr_inv, 3);
				grav[i].acc -= m_dr3_inv * dr;
				grav[i].pot -= jth.mass * dr_inv;
			}
			grav[i].pot += ith.mass/sqrt(eps2);
		}
	}
};

int main(int argc, char *argv[]){
	std::cout << std::scientific << std::setprecision(16);
	std::cerr << std::scientific << std::setprecision(16);
	PS::Initialize(argc, argv);
	//device.initialize();
	PS::ParticleSystem<FPGrav> ptcl;
	ptcl.initialize();
	PS::TreeForForceLong<ForceGrav, EPIGrav, EPJGrav>::Monopole tree_grav;
	PS::DomainInfo dinfo;

	SetIC(ptcl);
	dinfo.initialize(0.3);
	ptcl.setAverageTargetNumberOfSampleParticlePerProcess(200);
	dinfo.collectSampleParticle(ptcl);
	dinfo.decomposeDomainAll(ptcl);
	ptcl.exchangeParticle(dinfo);
	tree_grav.initialize(ptcl.getNumberOfParticleLocal(), 0.5, 8, 128);

	tree_grav.calcForceAllAndWriteBackMultiWalk(DispatchKernel, RetrieveKernel, 1, ptcl, dinfo, N_WALK_LIMIT);
	ptcl.writeParticleAscii("pezy.dat");
	tree_grav.calcForceAllAndWriteBack(CalcGravityForce<EPJGrav>(), CalcGravityForce<PS::SPJMonopole>(), ptcl, dinfo);
	ptcl.writeParticleAscii("cpu.dat");
	PS::Finalize();
	return 0;
}

