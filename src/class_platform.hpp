#pragma once


//Self gravity
class ForceGrav{
	public:
	PS::F64vec acc;
	PS::F64    pot;
	void clear(){
		acc = 0.0;
		pot = 0.0;
	}
};


class FPGrav{
public:
	PS::S64    id;
	PS::F64    mass;
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64vec acc;
	PS::F64    pot;

	PS::F64vec getPos() const{
		return pos;
	}
	PS::F64 getCharge() const{
		return mass;
	}
	void copyFromForce(const ForceGrav& force){
		acc = force.acc;
		pot = force.pot;
	}
	void writeAscii(FILE* fp) const{
		fprintf(fp, "%lld\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\n", id, mass, pos.x, pos.y, pos.z, vel.x, vel.y, vel.z, acc.x, acc.y, acc.z, pot);
	}
};

class EPIGrav{
	public:
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64    mass;
	PS::F64vec getPos() const{
		return pos;
	}
	void copyFromFP(const FPGrav& rp){
		pos  = rp.pos;
		vel  = rp.vel;
		mass = rp.mass;
	}
};

class EPJGrav{
	public:
	PS::F64vec pos;
	PS::F64vec vel;
	PS::F64    mass;
	PS::F64vec getPos() const{
		return pos;
	}
	PS::F64 getCharge(void) const{
		return mass;
	}
	void copyFromFP(const FPGrav& rp){
		pos  = rp.pos;
		vel  = rp.vel;
		mass = rp.mass;
	}
};


