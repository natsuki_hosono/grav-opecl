#include <iostream>
#include <iomanip>
#include <vector>
#include <assert.h>
#include <cmath>
#include <sys/time.h>

#include <particle_simulator.hpp>
#include <pzcl/pzcl_ocl_wrapper.h>
#include <PZSDKHelper.h>
#include "class_platform.hpp"
#include "class_device.hpp"

#include "pezy.hpp"

struct{
	int j_disp[N_WALK_LIMIT+2];
	EpiDev epi[NI_LIMIT];
	EpjDev epj[NJ_LIMIT];
	ForceDev force[NI_LIMIT];
}host;

static bool LoadFile(const char* name, size_t size, char* pData){
	FILE* fp = fopen(name, "rb");
	if(fp == NULL){
		printf("can not open %s\n", name);
		return false;
	}

	if(size == 0 || pData == NULL){
		printf("invalid params %s\n", __FUNCTION__);
		return false;
	}

	size_t size_ret = fread(pData, sizeof(char), size, fp);
	fclose(fp);

	if(size_ret != size){
		printf("can not read requested size\n");
		return false;
	}
	return true;
}

static size_t GetFileSize(const char* name){
	FILE* fp = fopen(name, "rb");
	if(fp == NULL){
		printf("can not open %s", name);
		return 0;
	}
	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	fclose(fp);
	return size;
}

cl_program CreateProgram(cl_context context, std::vector<cl_device_id> &device_id_list, const char* bin_name){
	cl_program program = NULL;
	char* pBin = NULL;
	cl_int result;

	size_t sizeFile = GetFileSize(bin_name);
	if(sizeFile == 0){
		goto leaving;
	}

	PZSDK_ALIGNED_ALLOC(pBin, sizeFile, 8 /*8 byte alignment*/);
	if(pBin == NULL){
		printf("out of host memory\n");
		goto leaving;
	}

	if(!LoadFile(bin_name, sizeFile, pBin)){
		goto leaving;
	}

	{
		const unsigned char* listBin[1];
		listBin[0] = (unsigned char*)pBin;
		cl_int binary_status = CL_SUCCESS;
		size_t length = sizeFile;

		program = clCreateProgramWithBinary(context, (cl_uint)device_id_list.size(), &device_id_list[0], &length, listBin, &binary_status, &result);
	}
	if(program == NULL){
		printf("clCreateProgramWithBinary failed, %d\n", result);
		goto leaving;
	}
leaving:
	if(pBin){
		PZSDK_ALIGNED_FREE(pBin);
	}
	return program;
}

struct PezyDevice{
	static const int NumDeviceMax = 8;
	//on device vars.
	cl_mem j_disp;
	cl_mem epi;
	cl_mem epj;
	cl_mem force;
	//device info.
	cl_platform_id   platform_id;
	cl_uint          num_of_platforms, num_of_devices;
	cl_device_id     device_id[NumDeviceMax];
	cl_context       context;
	cl_command_queue cmd_queue;
	cl_program       program;
	cl_kernel        kernel;

	std::vector<cl_device_id> device_id_list;

	void initialize(){
		cl_int return_code;
		int rank = PS::Comm::getRank();
		return_code = clGetPlatformIDs(1, &platform_id, &num_of_platforms);
		std::cout << "clGetPlatformIDs at #" << rank << ": " << return_code << std::endl;
		return_code = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, NumDeviceMax, device_id, &num_of_devices);
		std::cout << "clGetDeviceIDs at #" << rank << ": " << return_code << std::endl;
		context     = clCreateContext(NULL, 1, &device_id[rank % NumDeviceMax], NULL, NULL, &return_code);
		std::cout << "clCreateContext at #" << rank << ": " << return_code << std::endl;
		cmd_queue   = clCreateCommandQueue(context, device_id[rank % NumDeviceMax], 0, &return_code);
		std::cout << "clCreateCommandQueue at #" << rank << ": " << return_code << std::endl;

		std::cout << "Platform(" << platform_id << ") # " << num_of_platforms << std::endl;
		std::cout << "Device  (" << device_id[rank]   << ") # " << num_of_devices << std::endl;

		j_disp = clCreateBuffer(context, CL_MEM_READ_WRITE, (N_WALK_LIMIT + 2) * sizeof(int), NULL, &return_code);
		std::cout << "clCreateBuffer at #" << rank << ": " << return_code << std::endl;
		epi    = clCreateBuffer(context, CL_MEM_READ_WRITE, NI_LIMIT * sizeof(EpiDev),        NULL, &return_code);
		std::cout << "clCreateBuffer at #" << rank << ": " << return_code << std::endl;
		epj    = clCreateBuffer(context, CL_MEM_READ_WRITE, NJ_LIMIT * sizeof(EpjDev),        NULL, &return_code);
		std::cout << "clCreateBuffer at #" << rank << ": " << return_code << std::endl;
		force  = clCreateBuffer(context, CL_MEM_READ_WRITE, NI_LIMIT * sizeof(ForceDev),      NULL, &return_code);
		std::cout << "clCreateBuffer at #" << rank << ": " << return_code << std::endl;

		device_id_list.push_back(device_id[rank % NumDeviceMax]);
		program = CreateProgram(context, device_id_list, "./kernel.sc2/kernel.pz");
		if(program == NULL){
			std::cerr << "can't create program" << std::endl;
			exit(1);
		}

		kernel = clCreateKernel(program, "GravityKernel", &return_code);
		if(kernel == NULL){
			std::cerr<<"can't create kernel"<<std::endl;
			exit(1);
		}
		//return_code = clSetKernelArg(kernel, 4, sizeof(int), (void*)&N_THREAD_MAX);
	}
	~PezyDevice(){
		clReleaseMemObject(j_disp);
		clReleaseMemObject(epi);
		clReleaseMemObject(epj);
		clReleaseMemObject(force);

		clFlush(cmd_queue);
		clFinish(cmd_queue);
		clReleaseKernel(kernel);
		clReleaseProgram(program);
		clReleaseCommandQueue(cmd_queue);
		clReleaseContext(context);
	}
}device;

PS::S32 DispatchKernel(const PS::S32 tag, const PS::S32 n_walk, const EPIGrav* epi[], const PS::S32 Nepi[], const EPJGrav* epj[], const PS::S32 Nepj[], const PS::SPJMonopole* spj[], const PS::S32 Nspj[]){
	static bool isFirst = true;
	if(isFirst == true){
		device.initialize();
		isFirst = false;
	}
	
	assert(n_walk <= N_WALK_LIMIT);
	cl_int return_code;
	PS::S32 ni_tot = 0;
	host.j_disp[0] = 0;
	for(int k = 0 ; k < n_walk ; ++ k){
		ni_tot += Nepi[k];
		host.j_disp[k + 1] = host.j_disp[k] + (Nepj[k] + Nspj[k]);
	}
	host.j_disp[n_walk + 1] = host.j_disp[n_walk];
	assert(ni_tot < NI_LIMIT);
	assert(host.j_disp[n_walk] < NJ_LIMIT);

	return_code = clEnqueueWriteBuffer(device.cmd_queue, device.j_disp, CL_TRUE, 0, (n_walk + 2) * sizeof(int), host.j_disp, 0, NULL, NULL);
	ni_tot = 0;
	int nj_tot = 0;
	for(int iw = 0 ; iw < n_walk ; ++ iw){
		for(int i = 0 ; i < Nepi[iw] ; ++ i){
			host.epi[ni_tot].rx   = epi[iw][i].pos.x;
			host.epi[ni_tot].ry   = epi[iw][i].pos.y;
			host.epi[ni_tot].rz   = epi[iw][i].pos.z;
			host.epi[ni_tot].mass = epi[iw][i].mass;
			host.epi[ni_tot].id_walk = iw;
			++ ni_tot;
		}
		for(int j = 0 ; j < Nepj[iw] ; ++ j){
			host.epj[nj_tot].rx   = epj[iw][j].pos.x;
			host.epj[nj_tot].ry   = epj[iw][j].pos.y;
			host.epj[nj_tot].rz   = epj[iw][j].pos.z;
			host.epj[nj_tot].mass = epj[iw][j].mass;
			++ nj_tot;
		}
		for(int j = 0 ; j < Nspj[iw] ; ++ j){
			host.epj[nj_tot].rx   = spj[iw][j].pos.x;
			host.epj[nj_tot].ry   = spj[iw][j].pos.y;
			host.epj[nj_tot].rz   = spj[iw][j].pos.z;
			host.epj[nj_tot].mass = spj[iw][j].mass;
			++ nj_tot;
		}
	}
	return_code = clEnqueueWriteBuffer(device.cmd_queue, device.epi, CL_TRUE, 0, (ni_tot) * sizeof(EpiDev), host.epi, 0, NULL, NULL);
	return_code = clEnqueueWriteBuffer(device.cmd_queue, device.epj, CL_TRUE, 0, (nj_tot) * sizeof(EpjDev), host.epj, 0, NULL, NULL);

	return_code = clSetKernelArg(device.kernel, 0, sizeof(cl_mem), (void*)&device.j_disp);
	return_code = clSetKernelArg(device.kernel, 1, sizeof(cl_mem), (void*)&device.epi);
	return_code = clSetKernelArg(device.kernel, 2, sizeof(cl_mem), (void*)&device.epj);
	return_code = clSetKernelArg(device.kernel, 3, sizeof(cl_mem), (void*)&device.force);
	return_code = clSetKernelArg(device.kernel, 4, sizeof(int)   , (void*)&ni_tot);

	size_t work_size = N_THREAD_MAX;
	return_code = clEnqueueNDRangeKernel(device.cmd_queue, device.kernel, 1, NULL, &work_size, NULL, 0, NULL, NULL);
	return 0;
}

PS::S32 RetrieveKernel(const PS::S32 tag, const PS::S32 n_walk, const PS::S32 ni[], ForceGrav* force[]){
	cl_int return_code;
	int ni_tot = 0;
	for(int k = 0 ; k < n_walk ; ++ k){
		ni_tot += ni[k];
	}
	return_code = clEnqueueReadBuffer(device.cmd_queue, device.force, CL_TRUE, 0, ni_tot * sizeof(ForceDev), host.force, 0, NULL, NULL);
	int cnt = 0;
	for(int w = 0 ; w < n_walk ; ++ w){
		for(int i = 0;  i < ni[w] ; ++ i){
			force[w][i].acc.x = host.force[cnt].ax;
			force[w][i].acc.y = host.force[cnt].ay;
			force[w][i].acc.z = host.force[cnt].az;
			force[w][i].pot   = host.force[cnt].pot;
			++ cnt;
		}
	}
	return 0;
}

