#include <pzc_builtin.h>
#include "../class_device.hpp"

float fast_rsqrt(const float x) {
	int i = *(int*)&x;
	i = 0x5f3759df - (i >> 1);
	const float y = *(float*)&i;
	return y * (1.5f - 0.5f * x * y * y);
}

void pzc_GravityKernel(const int* const j_disp, const EpiDev* const epi, const EpjDev* const epj, ForceDev* force, const int n_total){
	const real eps2 = 1.0/32.0 * 1.0/32.0;
	const int tid = get_tid();
	const int pid = get_pid();
	const int index_offset = pid * get_maxtid() + tid;
	const int index_base   = get_maxtid() * get_maxpid();
	//typedef double vec4 __attribute__((ext_vector_type(4)));
	for(int i = index_offset ; i < n_total ; i += index_base){
		const EpiDev& ith = epi[i];
		real ax, ay, az, pot;
		ax = ay = az = pot = 0.0;
		const int id_walk = ith.id_walk;
		const int j_head = j_disp[id_walk];
		const int j_tail = j_disp[id_walk + 1];
		for(int j = j_head ; j < j_tail ; ++ j){
			const EpjDev& jth = epj[j];
			const real drx = jth.rx - ith.rx;
			const real dry = jth.ry - ith.ry;
			const real drz = jth.rz - ith.rz;
			const real r2       = drx * drx + dry * dry + drz * drz + eps2;
			const real r_inv    = 1.0/sqrt(r2);
			//const real r_inv    = fast_rsqrt(r2);
			const real m_r_inv  = jth.mass * r_inv;
			const real m_r3_inv = m_r_inv * r_inv * r_inv;
			pot -= m_r_inv;
			ax += m_r3_inv * drx;
			ay += m_r3_inv * dry; 
			az += m_r3_inv * drz;
		}
		force[i].pot = pot + ith.mass / sqrt(eps2);
		force[i].ax = ax;
		force[i].ay = ay;
		force[i].az = az;
	}
	flush();
}

